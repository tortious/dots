<div align="center">
    <h2><b>Current setup</b></h2>
    <img src="https://i.imgur.com/Niler8f.png">
    <br>
</div>

---

<div align="center">
    <h2><b>Details</b></h2>
</div>

https://i.imgur.com/5WGadF6.png

**Other screenshot with Firefox and Dolphin:** https://i.imgur.com/rFOudEQ.png

---

**Distribution:** Arch (GNU + Linux)

**Window Manager:** Xfwm4 on Xfce4

**Colour Scheme:** [Frost](https://gitlab.com/GaugeK/dots/raw/master/colours/Frost.png) [Xresources](https://gitlab.com/GaugeK/dots/raw/master/.config/Xres.Frost) (Needs a name)

**Qt Widget Style:** [Kvantum](https://github.com/tsujan/Kvantum/tree/master/Kvantum)

**Kvantum Theme:** Frost

**Icon Theme:** [Papirus-Dark](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)

**Wallpaper:** [Moon](https://i.imgur.com/IzTNKfp.png) 

**WM Theme:** [Frost](https://gitlab.com/GaugeK/dots/tree/master/.themes/Frost)

**GTK Theme:** Frost \(Generated with [oomox](https://github.com/themix-project/oomox)\)

**Cursor Theme:** Bibata Oil

**Panel:** tint2

**Font:** Roboto Condensed

**Terminal Font:** Iosevka Term  (Nerd font)

**Shell:** Zsh

**Shell Prompt:** [Slight](https://gitlab.com/GaugeK/dots/blob/master/bin/slight.zsh) 

**Terminal:** [Simple Terminal](https://gitlab.com/GaugeK/st)

---

**Keybinds:**

**Super + Return:** Open terminal emulator (st)

**Super + Space:** Show rofi

**Super + L:** Lock the screen (i3lock-color)

**Print:** Fullscreen screenshot using maim [link](https://gitlab.com/GaugeK/dots/blob/master/bin/maim_fullscreen)

**Shift + Print:** Screenshot a selection with maim [link](https://gitlab.com/GaugeK/dots/blob/master/bin/maim_selection)

**Super + Print:** Screenshot active window using maim [link](https://gitlab.com/GaugeK/dots/blob/master/bin/maim_focused)

**Ctrl + Alt + Insert:** Record the screen with [this script](https://gitlab.com/GaugeK/dots/blob/master/bin/record) (Displays dmenu prompts)

**Ctrl + Alt + End:** Stop recording

**Super + Alt + T:** Turn the touchpad off (No, I'm not on a laptop, but I use the same dotfiles on one)

**Ctrl + Alt + T:** Turns the touchpad back on 

**Ctrl + Alt + W:** Reload networking (My internet is unstable, because australia)

**Super + F4:** Kill active window (`xdotool getwindowfocus windowkill`)

**Super + Alt + M:** Open dmenu mount script 

**Super + Alt + U:** Open dmenu unmount script

**Super + F:** Open Firefox

**Super + C:** Open Dolphin

**Super + U:** Open rofi emoji script (Stolen from [here](https://gist.github.com/xywei/1e2c8a2fa6f17ba3f61b2e0d9f17bb7a))

**Ctrl + Alt + S:** Run my trash [Theme Switcher](https://gitlab.com/GaugeK/dots/blob/master/bin/ts.sh)   (WARNING! BAD CODE!)

**Super + T:** Toggle the panel \([Script](https://gitlab.com/GaugeK/dots/blob/master/bin/tp.sh)\)  -  Also mapped to Super + Shift + F1, which I have a macro on my mouse for.

Media keys use [this wrapper script](https://gitlab.com/GaugeK/dots/blob/master/bin/vol)

