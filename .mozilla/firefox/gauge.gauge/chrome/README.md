userChrome.css | Mostly [Responsive Minimal Firefox (r/FirefoxCSS)](https://www.reddit.com/r/FirefoxCSS/comments/8j0tek/responsive_minimal_firefox/), a few other minor additions, like hide bookmarks bar until hover (Can't find source)

userContent.css | [Dark scrollbar (r/FirefoxCSS)](https://www.reddit.com/r/FirefoxCSS/comments/a7nj0l/code_to_make_scrollbar_dark/ec4hkj4/)

about_page_scrollbars.css | same scrollbar but on native firefox pages also from [the last thread linked](https://www.reddit.com/r/FirefoxCSS/comments/a7nj0l/code_to_make_scrollbar_dark/ec4ivj7/)